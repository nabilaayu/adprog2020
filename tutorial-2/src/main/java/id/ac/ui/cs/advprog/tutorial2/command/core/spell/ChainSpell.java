package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells;
    // TODO: Complete Me
    public ChainSpell(ArrayList spells){
        this.spells = spells;
    }

    public void cast(){
        for(Spell spell: this.spells){
            spell.cast();
        }
    }

    public void undo(){
        for(int i = this.spells.size()-1; i>=0; i--){
            this.spells.get(i).undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
