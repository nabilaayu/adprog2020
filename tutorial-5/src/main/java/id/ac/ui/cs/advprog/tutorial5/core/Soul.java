package id.ac.ui.cs.advprog.tutorial5.core;

import javax.persistence.*;

@Entity
@Table(name = "soul")
public class Soul {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "name")
    private String name;
    @Column(name = "age")
    private int age;
    @Column(name = "gender")
    private String gender;
    @Column(name = "occupation")
    private String occupation;

    public Soul(){};

    public Soul(String name, int age, String gender, String occupation){
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.occupation = occupation;
    }

    public long getId(){
        return this.id;
    }
    public String getName(){
        return this.name;
    }
    public int getAge(){
        return this.age;
    }
    public String getGender(){
        return this.gender;
    }
    public String getOccupation(){
        return this.occupation;
    }

    public void setId(long id){
        this.id = id;
    }
    public void setName(String name){
        this.name = name;
    }
    public void setAge(int age){
        this.age = age;
    }
    public void setGender(String gender){
        this.gender = gender;
    }
    public void setOccupation(String occupation){
        this.occupation = occupation;
    }



    // TODO: Lengkapi atribut yang sudah anda rencanakan. Ingat bahwa atribut yang dibuat bersifat privat.
}

