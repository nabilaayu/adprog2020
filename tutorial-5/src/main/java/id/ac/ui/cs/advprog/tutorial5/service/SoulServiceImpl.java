package id.ac.ui.cs.advprog.tutorial5.service;
import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.springframework.stereotype.Service;
import id.ac.ui.cs.advprog.tutorial5.repository.SoulRepository;

import java.util.List;
import java.util.Optional;

// TODO: Import service bean
@Service
public class SoulServiceImpl implements SoulService {
    // TODO: implementasi semua method di SoulService.java. Coba lihat dokumentasi JpaRepository untuk mengimplementasikan Service
    public SoulRepository soulRepository;

    public SoulServiceImpl(SoulRepository soulRepository){
        this.soulRepository = soulRepository;
    }

    public List<Soul> findAll(){
        return soulRepository.findAll();
    };
    public Optional<Soul> findSoul(Long id){
        return soulRepository.findById(id);
    };
    public void erase(Long id){
        soulRepository.deleteById(id);
    }; //delete
    public Soul rewrite(Soul soul){
        soulRepository.save(soul);
        return soul;
    }; //update
    public Soul register(Soul soul){
        soulRepository.save(soul);
        return soul;
    }; //create
}
