import id.ac.ui.cs.advprog.tutorial5.core.Soul;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
class SoulTest {

    private Soul soul;

    @BeforeEach
    public void setUp(){
        soul = new Soul("ina", 30, "M", "Knight");
        soul.setId(1);
    }

    @Test
    void getGender() {
        assertEquals("M",soul.getGender());
    }

    @Test
    void setGender() {
        soul.setGender("L");
        assertEquals("L",soul.getGender());
    }

    @Test
    void getOccupation() {
        assertEquals("Knight",soul.getOccupation());
    }

    @Test
    void setOccupation() {
        soul.setOccupation("Hero");
        assertEquals("Hero",soul.getOccupation());
    }

    @Test
    void getName() {
        assertEquals("ina",soul.getName());
    }

    @Test
    void setName() {
        soul.setName("ani");
        assertEquals("ani",soul.getName());
    }

    @Test
    void getAge() {
        assertEquals(30,soul.getAge());
    }

    @Test
    void setAge() {
        soul.setAge(1);
        assertEquals(1,soul.getAge());
    }

    @Test
    void getId() {
        assertEquals(1,soul.getId());
    }

    @Test
    void setId() {
        soul.setId(2);
        assertEquals(2,soul.getId());
    }
}





