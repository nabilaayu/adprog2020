package id.ac.ui.cs.advprog.tutorial4.singleton.core;

public class HolyWish {

    private String wish;

    // TODO complete me with any Singleton approach

    private static HolyWish holy;

    private HolyWish(){}

    public static HolyWish getInstance(){
        if(holy == null){
            holy = new HolyWish();
        }
        return holy;
    }

    public String getWish() {
        return wish;
    }

    public void setWish(String wish) {
        this.wish = wish;
    }

    @Override
    public String toString() {
        return wish;
    }
}
