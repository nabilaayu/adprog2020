package id.ac.ui.cs.advprog.tutorial4.singleton.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class HolyGrailTest {

    // TODO create tests

    HolyGrail holyGrail;

    @BeforeEach
    public void setUp() throws Exception{
        holyGrail = new HolyGrail();
    }

    @Test
    public void testMakeAWish(){
        holyGrail.makeAWish("A wish");
        assertEquals("A wish", holyGrail.getHolyWish().getWish());
    }
}
